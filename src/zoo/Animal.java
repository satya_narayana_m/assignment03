/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zoo;

/**
 *
 * @author satyanarayana Madala
 */
public abstract class Animal {
    String species;
    char gender;

    public Animal(String species, char gender) {
        this.species = species;
        this.gender = gender;
    }

    public String getSpecies() {
        return species;
    }

    public char getGender() {
        return gender;
    }
    
    public abstract Animal replicate();

    
}
