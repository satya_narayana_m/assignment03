/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author satyanarayana Madala
 */
public class AnimalDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        Scanner scan = new Scanner(new File("animals.txt"));
        Zoo zoo=new Zoo();
        while (scan.hasNext()) {
            String animalType = scan.next();
            String species = scan.next();
            char gender = scan.next().charAt(0);
            if (animalType.equals("Carnivore")) {
                //Carnivore carnivore = new Carnivore(species, gender);
                zoo.addAnimalToZoo(new Carnivore(species, gender));
            } else {
                zoo.addAnimalToZoo(new Herbivore(species, gender));
            }

        }
        PrintWriter printing=new PrintWriter("output.txt");
        printing.println("Printing Zoo after adding all the animals to zoo from the input file");
        zoo.printZoo(printing);
        printing.println();
          printing.println("Printing Zoo after arranging all animals using bestFitting");
          printing.println();
        //System.out.println("\nPrinting Zoo after arranging all animals using bestFitting");
        zoo.zooBestFitting();
        zoo.printZoo(printing);
        
        scan=new Scanner(new File("animalsFood.txt"));
        ArrayList<Herbivore> carnivoreFood=new ArrayList<>();
        while (scan.hasNext()) {
            carnivoreFood.add(new Herbivore(scan.next(), 'M'));
            
        }          
////        //
        
        //System.out.println("\nPrinting Zoo after adding food for the carnivorous animals\n");
        printing.println();
        printing.println("Printing Zoo after adding food for the carnivorous animals");
        printing.println();
        zoo.addFood(carnivoreFood);
        zoo.printZoo(printing);
        //System.out.println("\nPrinting Zoo after carnivorous animals prey on animals given as food\n");
        printing.println();
        printing.println("Printing Zoo after carnivorous animals prey on animals given as food");
        printing.println();
        zoo.prey();
        zoo.printZoo(printing);
        printing.println();
        printing.println("Printing Zoo after animals with same species and opposite gender breed");
        printing.println();
        //System.out.println("\nPrinting Zoo after animals with same species and opposite gender breed\n");
        zoo.breeding();
        zoo.printZoo(printing);
        printing.close();
    }

}
