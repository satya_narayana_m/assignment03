/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author satyanarayana Madala
 */
public class Zoo {

    ArrayList<Animal> cages[][] = new ArrayList[4][4];
    final int MAX_PARENT_ANIMALS_PER_CAGE = 2;
    final int MAX_CUBS = 3;
    final int SEED_VALUE = 9999;
    final int MAX_TOTAL_ANIMALS_PER_CAGE = 6;

    public void addAnimalToZoo(Animal animal) {
        for (ArrayList<Animal>[] cage : cages) {
            for (int j = 0; j < cage.length; j++) {
                if (null == cage[j]) {
                    cage[j] = new ArrayList<>();
                    cage[j].add(animal);
                    return;
                } else {
                    if (cage[j].size() < MAX_PARENT_ANIMALS_PER_CAGE) {
                        if (animal instanceof Carnivore && cage[j].get(0) instanceof Carnivore) {
                            cage[j].add(animal);
                            return;
                        } else if (animal instanceof Herbivore && cage[j].get(0) instanceof Herbivore) {
                            cage[j].add(animal);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void printZoo(PrintWriter printing) {
    //public void printZoo() {
        for (int i = 0; i < cages.length; i++) {
            for (int l = 0; l < 15 * cages.length + cages.length+1; l++) {
                printing.print("-");
            }
            printing.println("");
            // printing.println("-----------------------------------------------------------------");
           // System.out.println("-----------------------------------------------------------------");
            for (int count = 0; count < MAX_TOTAL_ANIMALS_PER_CAGE; count++) {
                for (int j = 0; j < cages[i].length; j++) {
                    if (cages[i][j].size() > count) {
                          printing.print(String.format("|%-15s", cages[i][j].get(count).getSpecies()+" "+cages[i][j].get(count).getGender()));
                     //   System.out.print(String.format("|%-15s", cages[i][j].get(count).getSpecies() + " " + cages[i][j].get(count).getGender()));//
                    } else {
                           printing.print(String.format("|%-15s", ""));
                        //System.out.print(String.format("|%15s", ""));
                    }
                }
                 printing.println("|");
                //System.out.println("|");
            }
        }
         //printing.println("-----------------------------------------------------------------");
                    for (int l = 0; l < 15 * cages.length + cages.length+1; l++) {
                printing.print("-");
            }
            printing.println("");
        //System.out.println("-----------------------------------------------------------------");
    }

    public int[] findBestPairIndexes(int outerIndex, int innerIndex, int animalNumber) {
    
        for (int i = outerIndex; i < cages.length; i++) {
            for (int j = 0; j < cages[i].length; j++) {
                for (int k = 0; k < cages[i][j].size(); k++) {
                    if (i != outerIndex || j != innerIndex || k != animalNumber) {
                        if (cages[outerIndex][innerIndex].get(animalNumber).getSpecies().equals(cages[i][j].get(k).getSpecies())
                                && cages[outerIndex][innerIndex].get(animalNumber).getGender()!= cages[i][j].get(k).getGender()) {
                            return new int[]{i,j,k};
                        }
                    }
                }
            }
        }

        for (int i = outerIndex; i < cages.length; i++) {
            for (int j = 0; j < cages[i].length; j++) {
                for (int k = 0; k < cages[i][j].size(); k++) {
                    if (i != outerIndex || j != innerIndex || k != animalNumber) {
                        if (cages[outerIndex][innerIndex].get(animalNumber).getSpecies().equals(cages[i][j].get(k).getSpecies())) {
                            return new int[]{i,j,k};
                        }
                    }
                }
            }
        }
        return new int[]{outerIndex, innerIndex, animalNumber};
    }

    public void zooBestFitting() {
        
        for (int i = 0; i < cages.length; i++) {
            for (int j = 0; j < cages[i].length; j++) {
                for (int k = 0; k < cages[i][j].size(); k++) {
                    if (cages[i][j].size() <= 2) {
                        int[] bestIndices = this.findBestPairIndexes(i, j, k);
                        if (i != bestIndices[0] || j != bestIndices[1] || k != bestIndices[2]) {
                            Animal animal = cages[i][j].get(k == 0 ? ++k : --k);
                            cages[i][j].set(k, cages[bestIndices[0]][bestIndices[1]].get(bestIndices[2]));
                            cages[bestIndices[0]][bestIndices[1]].set(bestIndices[2], animal);
                            break;
                        }
                    }
                }
            }
        }
    }

    public void breeding() {
        Random rand = new Random(SEED_VALUE);
        for (ArrayList<Animal>[] eachrow : cages) {
            for (ArrayList<Animal> eachcage : eachrow) {
                if (eachcage.size() == MAX_PARENT_ANIMALS_PER_CAGE) {
                    if (eachcage.get(0).getSpecies().equals(eachcage.get(1).getSpecies()) && eachcage.get(0).getGender() != eachcage.get(1).getGender()) {
                        
                        int cubs = rand.nextInt(MAX_CUBS)+1;
                        for (int i = 0; i < cubs; i++) {
                            if (eachcage.get(0).getGender() == 'F') {
                                eachcage.add(eachcage.get(0).replicate());
                            } else {
                                eachcage.add(eachcage.get(1).replicate());
                            }
                        }
                    }
                }
            }
        }
    }

    public void addFood(ArrayList<Herbivore> foodAnimals) {

        for (int i = 0, count = 0; i < cages.length; i++) {
            ArrayList<Animal>[] arrayLists = cages[i];
            for (int j = 0; j < arrayLists.length; j++) {
                if (arrayLists[j].get(0) instanceof Carnivore && count<foodAnimals.size()) {
                    arrayLists[j].add(foodAnimals.get(count));
                    count++;
                }
            }
        }
    }

    public void prey() {
        for (int i = 0; i < cages.length; i++) {
            ArrayList<Animal>[] arrayLists = cages[i];
            for (int j = 0; j < arrayLists.length; j++) {
                if (arrayLists[j].get(0) instanceof Carnivore) {
                    for (int k = 0; k < arrayLists[j].size(); k++) {
                        if (arrayLists[j].get(k) instanceof Herbivore) {
                            Carnivore carnivore=(Carnivore) cages[i][j].get(0);
                            carnivore.eat(cages, i,j,k);
                        }
                    }
                }
            }
        }
    }
}
