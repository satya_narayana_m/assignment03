/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package zoo;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author satyanarayana Madala
 */
public class Carnivore extends Animal{

    public Carnivore(String species, char gender) {
        super(species, gender);
    }

    @Override
    public Animal replicate() {
        
        Random rand= new Random();
        char childgender;
        if(rand.nextInt(100)<60){
            childgender='M';
        }else{
            childgender='F';
        }
        return new Carnivore(species.toLowerCase(), childgender);
    }
    public void eat(ArrayList<Animal>[][] animals, int outerIndex, int innerIndex, int edibleAnimalIndex) {
        
        animals[outerIndex][innerIndex].remove(edibleAnimalIndex);

    }
}
