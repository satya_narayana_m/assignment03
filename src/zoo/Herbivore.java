/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zoo;

import java.util.Random;

/**
 *
 * @author satyanarayana Madala
 */
public class Herbivore extends Animal {

    public Herbivore(String species, char gender) {
        super(species, gender);
    }

    @Override
    public Animal replicate() {
        Random rand = new Random();
        char childGender;
        if (rand.nextInt(100) < 40) {
            childGender = 'M';
        } else {
            childGender = 'F';
        }
        return new Herbivore(species.toLowerCase(), childGender);
    }

}
